import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:io';

class ChatPage extends StatefulWidget {
  const ChatPage({Key? key}) : super(key: key);

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  final _textController = new TextEditingController();
  final _focusNode = new FocusNode();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Column(children: [
            Center(
              child: CircleAvatar(
                child: Text('AM', style: TextStyle(fontSize: 12)),
                backgroundColor: Colors.blue.shade100,
                maxRadius: 14,
              ),
            ),
            SizedBox(height: 3),
            Text(
              'Adrian Madrigal',
              style: TextStyle(color: Colors.black87, fontSize: 12),
            )
          ]),
          centerTitle: true,
          elevation: 1,
        ),
        body: Container(
          child: Column(
            children: [
              Flexible(
                  child: ListView.builder(
                physics: BouncingScrollPhysics(),
                itemBuilder: (_, i) => Text('$i'),
                reverse: true,
              )),
              Divider(height: 1),
              Container(color: Colors.white, child: _inputChat())
            ],
          ),
        ));
  }

  Widget _inputChat() {
    return SafeArea(
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 8),
        child: Row(
          children: [
            Flexible(
                child: TextField(
              controller: _textController,
              onSubmitted: _handleSubmit,
              onChanged: (String texto) {
                //TODO:cuando hay un valor, para poder postear
              },
              decoration: InputDecoration.collapsed(hintText: 'Enviar mensaje'),
              focusNode: _focusNode,
            )),
            //Boton de enviar
            Container(
                margin: EdgeInsets.symmetric(horizontal: 4.0),
                child: Platform.isIOS
                    ? CupertinoButton(child: Text('Enviar'), onPressed: () {})
                    : Container(
                        margin: EdgeInsets.symmetric(horizontal: 4.0),
                        child: IconButton(
                            onPressed: () {},
                            icon: Icon(Icons.send, color: Colors.blue[400])),
                      ))
          ],
        ),
      ),
    );
  }

  _handleSubmit(String texto) {
    print(texto);
    _focusNode.requestFocus();
    _textController.clear();
  }
}
