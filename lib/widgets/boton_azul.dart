import 'package:flutter/material.dart';

class BotonAzul extends StatelessWidget {
  const BotonAzul({Key? key, 
   required this.text, 
   required this.onPress}) : super(key: key);
  final String text;
  final void Function() onPress;

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      onPressed: this.onPress,
      elevation: 2,
      highlightElevation: 5,
      color: Colors.blue,
      shape: StadiumBorder(),
      child: Container(
        width: double.infinity,
        height: 55,
        child: Center(
          child: Text(this.text,
              style: TextStyle(color: Colors.white, fontSize: 17)),
        ),
      ),
    );
  }
}
